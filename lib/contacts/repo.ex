defmodule Contacts.Repo do
  use Ecto.Repo, otp_app: :contacts
  use Scrivener, page_size: 9
end
