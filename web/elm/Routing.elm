module Routing exposing (..)

import Navigation
import UrlParser exposing (..)

-- TODO: check other routing/matching libraries

type Route
    = HomeIndexRoute
    | NotFoundRoute
    | ShowContactRoute Int


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map HomeIndexRoute <| s ""
        , map ShowContactRoute <| s "contacts" </> int
        ]


parse : Navigation.Location -> Route
parse location =
    case UrlParser.parsePath matchers location of
        Just route ->
            route

        Nothing ->
            NotFoundRoute


toPath : Route -> String
toPath route =
    case route of
        HomeIndexRoute ->
            "/"

        NotFoundRoute ->
            "/not-found"


        ShowContactRoute id ->
            "/contacts/" ++ toString id
