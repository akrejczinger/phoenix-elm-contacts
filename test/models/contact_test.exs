defmodule Contacts.ContactTest do
  use Contacts.ModelCase

  alias Contacts.Contact

  @valid_attrs %{birth_date: ~D[2010-04-17], email: "some email", first_name: "some first_name", gender: 42, headline: "some headline", last_name: "some last_name", location: "some location", phone_number: "some phone_number", picture: "some picture"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Contact.changeset(%Contact{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Contact.changeset(%Contact{}, @invalid_attrs)
    refute changeset.valid?
  end
end
