# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :contacts,
  ecto_repos: [Contacts.Repo]

# Configures the endpoint
config :contacts, Contacts.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+SWeZxBxW2a862U80j7+gCnVI+EEYmgtjTm90LIPXxlNcx7mVvkyNkRdRb9jq9B0",
  render_errors: [view: Contacts.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Contacts.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
